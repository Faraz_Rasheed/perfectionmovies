package com.mobXX.codechallenge.Interface

import com.mobXX.codechallenge.Services.DataModels.Movie
import com.mobXX.codechallenge.Services.Network.CustomRequest
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("movies")
    fun getMovies(): Call<Movie>
}

object ApiUtils {
    val apiService: ApiService
        get() = CustomRequest.getClient()!!.create(ApiService::class.java)


}