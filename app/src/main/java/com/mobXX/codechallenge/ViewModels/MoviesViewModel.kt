package com.mobXX.codechallenge.ViewModels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.mobXX.codechallenge.Services.DataModels.Movie
import com.mobXX.codechallenge.Services.Repository.MovieRepository
import android.R
import com.bumptech.glide.load.engine.DiskCacheStrategy

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           MoviesViewModel.kt                                      *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

class MoviesViewModel : ViewModel() {

    private var rootMoviesObservable: LiveData<Movie>? = null

    fun getMovies() {
        rootMoviesObservable = MovieRepository.getInstance()?.getMovies()
    }

    fun getMovieListObservable(): LiveData<Movie>? {
        return rootMoviesObservable
    }

    object BindingAdapters {
        @JvmStatic
        @BindingAdapter("bind:myTribeImage")
        fun loadImage(view: ImageView, imageUrl: String?) {
            if (imageUrl != null && !imageUrl.isEmpty()) {
                Glide
                    .with(view.context) // replace with 'this' if it's in activity
                    .load(imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(view)

            }
        }
    }
}