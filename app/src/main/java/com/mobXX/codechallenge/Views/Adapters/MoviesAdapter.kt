package com.mobXX.codechallenge.Views.Adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import com.mobXX.codechallenge.R
import com.mobXX.codechallenge.Data
import com.mobXX.codechallenge.databinding.RvCellMovieBinding
import java.util.*
import android.widget.Filterable

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           MoviesAdapter.kt                                      *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.ViewHolder> , Filterable {
    var moviesList: ArrayList<Data>
    lateinit var cleanerListFiltered: ArrayList<Data>
    lateinit var cleanerList: ArrayList<Data>
    lateinit var context: Context
    lateinit var binding: RvCellMovieBinding

    constructor(moviesList: ArrayList<Data>, context: Context) {
        this.moviesList = moviesList
        this.cleanerList = moviesList
        this.context = context
    }

    fun setList(tempList: ArrayList<Data>) {
        if (this.moviesList == null) {
            this.moviesList = tempList
            notifyItemRangeInserted(0, tempList.size)
        } else {
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int {
                    return this@MoviesAdapter.moviesList!!.size
                }

                override fun getNewListSize(): Int {
                    return tempList.size
                }

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return this@MoviesAdapter.moviesList!![oldItemPosition].id === tempList[newItemPosition].id

                }

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    val log = tempList[newItemPosition]
                    val old = tempList[oldItemPosition]
                    return log.id === old.id
                }
            })
            this.moviesList = tempList
            result.dispatchUpdatesTo(this)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
         binding = DataBindingUtil
            .inflate(
                LayoutInflater.from(parent.context), R.layout.rv_cell_movie,
                parent, false
            )
        return ViewHolder(binding)
    }

    /**
     * Setting view for each item
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        try {
            var movieData = moviesList[position]
            holder.binding.movie = movieData

        } catch (e: Exception) {
            Log.e("Exception", e.message)
        }

    }


    override fun getItemCount(): Int {
        return if (moviesList == null) 0 else moviesList!!.size
    }


    class ViewHolder(val binding: RvCellMovieBinding) : RecyclerView.ViewHolder(binding.getRoot())

    override public fun getFilter(): Filter {
        return object : Filter() {
            override protected fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    cleanerListFiltered = cleanerList
                } else {
                    val filteredList = ArrayList<Data>()
                    for (row in cleanerList) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.title.toLowerCase().contains(charString.toLowerCase())
                                || row.genre.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    cleanerListFiltered = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = cleanerListFiltered
                return filterResults
            }

            override protected fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {

                cleanerListFiltered = filterResults.values as ArrayList<Data>
                moviesList = cleanerListFiltered
                notifyDataSetChanged()
            }
        }
    }
}
