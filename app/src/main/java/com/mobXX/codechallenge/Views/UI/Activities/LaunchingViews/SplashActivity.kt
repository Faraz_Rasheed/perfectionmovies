package com.mobXX.codechallenge.Views.UI.Activities.LaunchingViews

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.mobXX.codechallenge.R
import com.mobXX.codechallenge.Utills.IntentCall
import com.mobXX.codechallenge.Views.UI.Activities.MoviesView.MoviesView
import com.mobXX.codechallenge.databinding.ActivityMainBinding

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           SplashActivity.kt                                     *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

class SplashActivity : AppCompatActivity() {

    lateinit var mainBinding : ActivityMainBinding
    lateinit var context: Context

    /**
     * Setting time for splash screen
     */
    private val SPLASH_DISPLAY_LENGTH = 1000 * 2



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        context = this

        initView()
    }

    private fun initView() {
        mainBinding.isVisible = false
        NavigateScreen()
    }


    /**
     * Navigate to next screen
     */
    private fun NavigateScreen() {
        YoYo.with(Techniques.FadeIn).withListener(object : com.nineoldandroids.animation.Animator.AnimatorListener {
            override fun onAnimationStart(animation: com.nineoldandroids.animation.Animator) {
               mainBinding.isVisible = true
            }

            override fun onAnimationEnd(animation: com.nineoldandroids.animation.Animator) {
                Handler().postDelayed({

                    /**
                     * Navigate to tutorial screen if new user
                     */
                    IntentCall.navigateToScreen<MoviesView>(true, this@SplashActivity)

                }, SPLASH_DISPLAY_LENGTH.toLong())

            }

            override fun onAnimationCancel(animation: com.nineoldandroids.animation.Animator) {

            }

            override fun onAnimationRepeat(animation: com.nineoldandroids.animation.Animator) {

            }
        }).duration(2000)
            .playOn(mainBinding.tvName)
        //
    }

}
