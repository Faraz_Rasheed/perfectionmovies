package com.mobXX.codechallenge.Views.UI.Activities.MoviesView

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager
import com.mobXX.codechallenge.Data
import com.mobXX.codechallenge.Services.DataModels.Movie
import com.mobXX.codechallenge.Utills.SpacesItemDecoration
import com.mobXX.codechallenge.ViewModels.MoviesViewModel
import com.mobXX.codechallenge.Views.Adapters.MoviesAdapter
import com.mobXX.codechallenge.databinding.ActivityMoviesViewBinding
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.view.Menu
import com.mobXX.codechallenge.R
import java.util.*


/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           MoviesVIew.kt                                         *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

class MoviesView : AppCompatActivity() {

    lateinit var binding: ActivityMoviesViewBinding
    lateinit var context: Context
    lateinit var model: MoviesViewModel
    lateinit var grigLayoutManager: GridLayoutManager
    lateinit var adapter: MoviesAdapter
    lateinit var searchView: SearchView
    var isLoading: Boolean = false

    var movieList: ArrayList<Data> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_movies_view)
        context = this

        initViews()
    }

    private fun initViews() {
        binding.mainLayout.isVisible = true
        binding.mainLayout.vwShimmerContainer.startShimmerAnimation()

        binding.mainLayout.swipeRefreshLayout.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            binding.mainLayout.swipeRefreshLayout.setRefreshing(true)
            isLoading = false
            sendRequest()
        })

        setAdapter()
        sendRequest()
    }

    private fun setAdapter() {
        var spacingInPixels: Int = getResources().getDimensionPixelSize(R.dimen.dp_10);
        binding.mainLayout.rvMovies.addItemDecoration(SpacesItemDecoration(spacingInPixels))
        grigLayoutManager = GridLayoutManager(context!!, 3)
        binding.mainLayout.rvMovies.layoutManager = grigLayoutManager
    }


    private fun sendRequest() {
        model = ViewModelProviders.of(this).get(MoviesViewModel::class.java!!)
        model.getMovies()
        observeJsonHomeViewModel(model, -1, null)
    }

    private fun observeJsonHomeViewModel(viewModel: MoviesViewModel, itemPosition: Int, movie: Movie?) {
        model.getMovieListObservable()?.observe(this, Observer<Movie> { homeModels ->

            binding.mainLayout.vwShimmerContainer.stopShimmerAnimation()
            binding.mainLayout.isVisible = false

            if (isLoading) {
                movieList.addAll(ArrayList<Data>(homeModels!!.data))
                isLoading = false
                adapter.notifyDataSetChanged()

            } else {
                movieList.clear()
                movieList.addAll(ArrayList<Data>(homeModels!!.data))
                adapter = MoviesAdapter(movieList, context)

                binding.mainLayout.rvMovies.adapter = adapter


            }
            if (binding.mainLayout.swipeRefreshLayout != null && binding.mainLayout.swipeRefreshLayout.isRefreshing()) {
                binding.mainLayout.swipeRefreshLayout.setRefreshing(false)  // This hides the spinner
            }
        })

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater

        inflater.inflate(com.mobXX.codechallenge.R.menu.menu, menu)
        val  searchViewItem = menu.findItem(com.mobXX.codechallenge.R.id.app_bar_search)

        searchView = MenuItemCompat.getActionView(searchViewItem) as SearchView


        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(onQueryTextListener);

        return super.onCreateOptionsMenu(menu)
    }

    private val onQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            adapter.filter.filter(query)
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            adapter.filter.filter(newText)

            return true
        }


    }




}
