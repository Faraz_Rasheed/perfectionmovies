package com.mobXX.codechallenge.Utills

import android.app.Activity
import android.content.Intent

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           IntentCall.kt                                         *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

object IntentCall {
    inline fun <reified T : Activity> navigateToScreen(isFinish: Boolean, activity: Activity) {
        val intent = Intent(activity, T::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(intent)
        if (isFinish) {
            activity.finish()
        }
    }
}