package com.mobXX.codechallenge.Utills

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           SpacesItemDecoration.kt                                         *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

class SpacesItemDecoration(private val space: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {

            outRect.left = space
            outRect.right = space

    }
}