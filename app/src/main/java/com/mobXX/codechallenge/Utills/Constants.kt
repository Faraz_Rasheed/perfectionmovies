package com.mobXX.codechallenge.Utills

/**
 ********************* Meta Data ***********************************
 *           Perfection Movies                                     *
 *           Constants.kt                                         *
 *                                                                 *
 *           Created by Faraz Rasheed on 21/03/2019                *
 *           Copyright © 2019 MobXX. All rights reserved.          *
 *******************************************************************
 */

object Constants {
     var baseUrl : String = "https://movies-sample.herokuapp.com/api/"
}