package com.mobXX.codechallenge.Services.Repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.mobXX.codechallenge.Interface.ApiService
import com.mobXX.codechallenge.Interface.ApiUtils
import com.mobXX.codechallenge.Services.DataModels.Movie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieRepository {
    companion object {
        var booksRepository: MovieRepository? = null
        @Synchronized
        fun getInstance(): MovieRepository? {
            if (booksRepository == null) {
                booksRepository = MovieRepository()
            }
            return booksRepository
        }
    }

    var mAPIService: ApiService? = null
    constructor() {
        mAPIService = ApiUtils.apiService
    }

    fun getMovies(): LiveData<Movie> {
        val data = MutableLiveData<Movie>()
        mAPIService = ApiUtils.apiService
        mAPIService?.getMovies()?.enqueue(object : Callback<Movie> {
            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                Log.i("resp", "post submitted to API." + response.body()!!)
                    data.value = response.body()!!

            }
            override fun onFailure(call: Call<Movie>, t: Throwable) {
                Log.e("Resp", t.message)
            }
        })
        return data
    }

}