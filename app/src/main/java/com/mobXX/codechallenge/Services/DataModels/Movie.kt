package com.mobXX.codechallenge.Services.DataModels

import com.google.gson.annotations.SerializedName
import com.mobXX.codechallenge.Data

data class Movie(
    @SerializedName("data")
    val `data`: List<Data>
)