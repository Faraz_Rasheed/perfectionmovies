package com.mobXX.codechallenge.Views.UI.Activities.MoviesView;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobXX.codechallenge.Data;
import com.mobXX.codechallenge.Views.Adapters.MoviesAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricTestRunner.class)
public class MoviesViewTest {

    MoviesView moviesView;
    MoviesAdapter adapter;
    ArrayList<Data> list;
    MoviesAdapter.ViewHolder holder;

    @Before
    public void setUp() throws Exception {
        moviesView = Robolectric.setupActivity(MoviesView.class);
        list = new ArrayList<>();
        list.add(new Data("A", 1,"topic1", "t1", "1"));
        list.add(new Data("A2", 2,"topic2", "t2", "1"));
        list.add(new Data("A3", 3,"topic3", "t3", "1"));

        adapter = new MoviesAdapter(list, moviesView);
        moviesView.binding.mainLayout.rvMovies.setLayoutManager(new GridLayoutManager(moviesView, 3));
        moviesView.binding.mainLayout.rvMovies.setAdapter(adapter);
        holder = adapter.onCreateViewHolder(moviesView.binding.mainLayout.rvMovies, 0);
    }

    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test_adapterTopicViewAt0() {
        adapter.onBindViewHolder(holder, 0);
        CardView mainLayout = (CardView) moviesView.binding.mainLayout.rvMovies.findViewHolderForAdapterPosition(0).itemView;

        ConstraintLayout child = (ConstraintLayout)  mainLayout.getChildAt(0);
        TextView tvTag = (TextView)  child.getChildAt(1);
        assertEquals("topic1" , tvTag.getText());
        assertEquals(View.VISIBLE, holder.getBinding().tvMoviegenre.getVisibility());
        assertEquals(adapter.getItemCount(),3);
    }
}
